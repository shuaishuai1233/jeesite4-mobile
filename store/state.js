import Config from '../common/js/config'

const state = {
    HOST_URL: Config.SERVER.HOST_URL,
	theme: Config.THEME,
	secretKey: "thinkgem,jeesite,com",
	__sid: "",
	hasLogin: true,
	loginPage: {
		needValid: false,
		input:{
			username: "system",
			password: "admin",
			validCode: ""
		},
		validCodeUrl: "",
		sid: ""
	},
	user:{},
	roles: [],
	stringPermissions:[]
}
export default state