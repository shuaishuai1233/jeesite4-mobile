import state from './state'

export const HOST_URL = state => state.HOST_URL
 
export const theme = state => state.theme

export const secretKey = state => state.secretKey

export const __sid = state => state.__sid

export const hasLogin = state => state.hasLogin

export const user = state => state.user

export const loginPage = state => state.loginPage