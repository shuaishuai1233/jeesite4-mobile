export default {
	// 可以以页面为单位来写，比如首页的内容，写在index字段，个人中心写在center，共同部分写在common部分
	"语言": "language",
	"设置": "setting",
	"返回": "back",
	"首页": "home",
	"通知": "notice",
	"我的": "mine",
	"主题": "theme",
	
	"嫣红": "red",
	"桔橙": "orange",
	"明黄": "yellow",
	"橄榄": "olive",
	"森绿": "green",
	"天青": "cyan",
	"海蓝": "blue",
	"姹紫": "purple",
	"木槿": "mauve",
	"桃粉": "pink",
	"棕褐": "brown",
	
	"登录": "signIn",
	"账号": "account",
	"密码": "password",
	"请输入账号": "input account",
	"请输入密码": "input password",
	"取消": "cancel",
	"验证码": "validCode"
}