// 主题
const THEME = {
	RED: '#e54d42', // 嫣红
	ORANGE: '#f37b1d', // 桔橙
	YELLOW:'#fbbd08', // 明黄
	OLIVE:'#8dc63f', // 橄榄
	GREEN:'#39b54a', // 森绿
	CYAN:'#1cbbb4', // 天青
	BLUE:'#0081ff', // 海蓝
	PURPLE:'#6739b6', // 姹紫
	MAUVE:'#9c26b0', // 木槿
	PINK:'#e03997', // 桃粉
	BROWN:'#a5673f', // 棕褐
	GREY:'#8799a3', // 玄灰
	GRAY:'#aaaaaa', // 草灰
    BLACK:'#333333', // 墨黑
	WHITE:'#ffffff' // 雅白
}
// 语言
const LANGUAGE = {
	ZH: "zh", // 中文  common/js/locales/zh.js
	EN: "en", // 英文  common/js/locales/en.js
}
export default {
	// 服务器请求相关配置
	SERVER:{
		// 服务器地址
		HOST_URL: "https://demo.jeesite.com/js/",
		// 默认请求方式
		METHOD: "POST",
		// 默认header
		HEADER:{
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		LOADING: "加载中",
		// 默认超时时间
		TIMEOUT: 60000,
		// 数据类型
		DATA_TYPE: "json",
		// 相应数据类型
		RESPONSE_TYPE: "text",
		// 验证ssl证书
		SSL_VERIFY: true,
		// 跨域请求时是否携带凭证
		WITH_CREDENTIALS: false,
		// DNS解析时优先使用IPV4
		FIRST_IPV4: false
	},
	// 登录提交信息安全Key 加密方式：DES
	SECRET_KEY: "xxzg",
	// 默认语言
	LANGUAGE: LANGUAGE.ZH,
	// 主题
	THEME: THEME.GREEN,
	// 缓存
	CATCH:{
		// 用户登录信息
		USER_LOGIN_INFO: "user_login_info"
	}
}