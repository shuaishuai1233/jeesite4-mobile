import Vue from 'vue'
import App from './App'
import store from './store'
// 语言初始化
import i18n from '@/common/js/language-init.js'
// HTTPAPI
import $request from '@/common/js/http.api.js'



import basics from './pages/basics/home.vue'
Vue.component('basics',basics)

import components from './pages/component/home.vue'
Vue.component('components',components)

import plugin from './pages/mine/home.vue'
Vue.component('plugin',plugin)

import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)

Vue.config.productionTip = false



// 由于微信小程序的运行机制问题，需声明如下一行，H5和APP非必填
Vue.prototype._i18n = i18n;

// 公共request请求
Vue.prototype.$request = $request;

App.mpType = 'app'

const app = new Vue({
	$request,
	i18n,
	store,
    ...App
})

app.$mount()

 



